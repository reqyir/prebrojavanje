package com.igorion.prebrojavanje.Model;

import android.content.Context;

import java.util.ArrayList;

public class ModelSlova {
    private String imeSlova;
    private int zvukSlovaRes;
    //private String slikaSlovaRes;
    private int pozicijaSlova;
    private ArrayList<String> azbukaNiz;
    Context context;

    public ModelSlova(){
        azbukaNiz=new ArrayList<String>(30);
		getAzbukaNiz();
    }

    public ArrayList<String> getAzbukaNiz() {
        azbukaNiz.add("broj1"); //0
        azbukaNiz.add("broj2"); //1
        azbukaNiz.add("broj3"); //2
        azbukaNiz.add("broj4"); //3
        azbukaNiz.add("broj5"); //4
        azbukaNiz.add("broj6"); //5
        azbukaNiz.add("broj7"); //6
        azbukaNiz.add("broj8"); //7
        azbukaNiz.add("broj9"); //8



        return azbukaNiz;
    }

    public int getPozicijaUNizu(String slovo){   // private int pozicijaSlova;
        pozicijaSlova = azbukaNiz.indexOf(slovo);
        return pozicijaSlova;
    }

    public String getImeSlova(int pozicija){
        imeSlova=azbukaNiz.get(pozicija);
        return imeSlova;
    }

    /*public int getZvukSlovaRes (int pozicija)    // private int zvukSlovaRes;
    {
        String pozicijaa = String.valueOf(pozicija);
        zvukSlovaRes = context.getResources().getIdentifier(this.getImeSlova(this.getPozicijaUNizu(pozicijaa)), "raw", MainActivity.PACKAGE_NAME);
        return zvukSlovaRes;
    }*/


}
