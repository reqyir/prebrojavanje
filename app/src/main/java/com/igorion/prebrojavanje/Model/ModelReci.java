package com.igorion.prebrojavanje.Model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ModelReci {
    private String originalRec;
    private String randomRec;
    private List<Character> recList=new ArrayList<Character>();
    private List<Character> shuffleRec=new ArrayList<Character>();

    public ModelReci(String rec){
        this.originalRec=new String(rec);
        napraviRecList();
        napraviShuffleRec();

    }

    private List<Character> napraviRecList(){

        for (int i = 0; i < originalRec.length(); i++){
            Character c = Character.toLowerCase(originalRec.charAt(i));
            recList.add(c);
        }
        return recList;
    }

    private void napraviShuffleRec(){

        this.shuffleRec = new ArrayList<Character>(this.recList);
        Collections.shuffle(shuffleRec);
    }

    /**
     * Pravi listu karaktera od originalne reci
     * @return List<Character> shuffleRec
     */
    public List<Character> ShuffleRecList(){
        return shuffleRec;
    }
    /**
     *
     * @return String originalna rec
     */
    public String OriginalRecStr(){
        return originalRec;
    }
    /**
     *
     * @return String shuffle rec
     */
    public String ShuffleRecStr(){
        StringBuilder builder = new StringBuilder(shuffleRec.size());
        for(Character ch: shuffleRec)
        {
            builder.append(ch);
        }
        return builder.toString();
    }

    public int getDuzinaReci(){
        return originalRec.length();
    }


}
