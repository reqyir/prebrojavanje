package com.igorion.prebrojavanje;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.igorion.prebrojavanje.Model.ModelSlova;

import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener {
    RelativeLayout rl, gornjiProstor;
    LinearLayout donjiProstor;
    int width, height, visinaIButtona, pomocniWidht;
    Button definisani;
    ImageView[] brojevi = new ImageView [10];
    ImageView[]cvetovi= new ImageView[10];
    int brojCvetova;
    Boolean [] cvetJeUBoji = new Boolean[10];
    int [] pozicijaX= new int [10];
    int [] pozicijaY = new int [10];
    ImageView probni, greska, bravo, refresh;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //sprecava portrait
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        rl = (RelativeLayout) findViewById(R.id.rl);
        rl.setBackgroundColor(Color.rgb(181,247,255));

        donjiProstor = (LinearLayout) findViewById(R.id.linear);
        donjiProstor.setBackgroundColor(Color.rgb(181,247,255));

        gornjiProstor = (RelativeLayout) findViewById(R.id.relative);
        gornjiProstor.setBackgroundColor(Color.rgb(181,247,255));

        definisani = (Button) findViewById(R.id.button2);

        Random r = new Random();
        brojCvetova = r.nextInt(9 - 1) + 1;
        for (int a = 0; a < 9; a++)
        {
            cvetJeUBoji[a] = false;
        }
        for (int a = 0; a<brojCvetova;a++)
        {
            cvetJeUBoji[a]=true;
        }
        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();  // deprecated kod mene je 1280
        height = display.getHeight();  // deprecated
        pomocniWidht = width - (8*16);
        visinaIButtona = pomocniWidht / 8;  // a nije visina nego sirina
        visinaIButtona = visinaIButtona - 16;



        rastrkajCvetove();
        postaviDonjiRed();
        iskljuciUkljuciButtone(false);  // iskljuceni brojevi
        //postaviProbni();
        staviCvetove();
    }

    //region Stavi cvetove
    private void staviCvetove()
    {
        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();  // deprecated kod mene je 1280
        int height = display.getHeight();  // deprecated
        pomocniWidht = width - (8*16);
        visinaIButtona = pomocniWidht / 8;  // a nije visina nego sirina
        visinaIButtona = visinaIButtona - 16;

        RelativeLayout.LayoutParams paramsZaCButton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsZaCButton.width=visinaIButtona;
        paramsZaCButton.height=visinaIButtona;
        paramsZaCButton.setMargins(5,5,0,0);

        for(int i=0; i<brojCvetova; i++){
            cvetovi[i] = new ImageView(this);
            cvetovi[i].setLayoutParams(paramsZaCButton);
            cvetovi[i].setImageResource(R.drawable.cvetuboji);
            cvetovi[i].setScaleType(ImageView.ScaleType.FIT_XY);
            cvetovi[i].setX(pozicijaX[i]);
            cvetovi[i].setY(pozicijaY[i]);
            final int finalI = i;
            cvetovi[i].setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v) {
                    cvetovi[finalI].setImageResource(R.drawable.cvetmono);
                    cvetJeUBoji[finalI] = false;
                    proveriBojuCvetova();
                }
            });
            gornjiProstor.addView(cvetovi[i]);
        }
    }
    ///endregion Stavi cvet

    //region getRes
    private int getRes(String name, String resName){
        return getResources().getIdentifier(name, resName, getPackageName());
    }
    //endregion getRes

    //region postaviDonjiRed()
    private void postaviDonjiRed()
    {
        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();  // deprecated kod mene je 1280
        int height = display.getHeight();  // deprecated
        pomocniWidht = width - (8*16);
        visinaIButtona = pomocniWidht / 8;  // a nije visina nego sirina
        visinaIButtona = visinaIButtona - 16;
        donjiProstor.removeView(definisani);

        ModelSlova modSlova = new ModelSlova();

        LinearLayout.LayoutParams paramsZaIButton = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        paramsZaIButton.height = visinaIButtona;
        paramsZaIButton.width = visinaIButtona;
        paramsZaIButton.setMargins(5,5,0,0);


        for(int i=0; i<9; i++){
            final String slovo = modSlova.getImeSlova(i);
            brojevi[i] = new ImageView(this);
            brojevi[i].setLayoutParams(paramsZaIButton);
            brojevi[i].setImageResource(getRes(slovo, "drawable"));
            final int finalI = i+1;
            brojevi[i].setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v) {
                    if (finalI ==brojCvetova)
                    {
                        String posalji = "bravo";
                        Ponovo(posalji);
                    }
                    else
                    {
                        String posalji = "greska";
                        Ponovo(posalji);
                    }
                    iskljuciUkljuciButtone(false);
                }
            });
            donjiProstor.addView(brojevi[i]);
        }
    }




    //endregion postaviDonjiRed

    //region iskljuciUkljuciButtone()
    private void iskljuciUkljuciButtone(Boolean staTreba)  /// false ih iskljucuje, a true ih ukljucuje
    {
        for(int i=0; i<9; i++){
            brojevi[i].setEnabled(staTreba);
        }
    }
    //endregion iskljuciUkljuciButtone()

    //region proveriBojuCvetova()
    private void proveriBojuCvetova()
    {
        if (cvetJeUBoji[0]==false&cvetJeUBoji[1]==false&cvetJeUBoji[2]==false&cvetJeUBoji[3]==false&cvetJeUBoji[4]==false&
                cvetJeUBoji[5]==false&cvetJeUBoji[6]==false&cvetJeUBoji[7]==false&cvetJeUBoji[8]==false)
        {iskljuciUkljuciButtone(true);}
    }
    //endregion proveriBojuCvetova

    //region rastrkajCvetove()
    private void rastrkajCvetove()
    {
        Random rnd = new Random();
        for (int i=0; i<brojCvetova;i++)
        {
            pozicijaX[i] = rnd.nextInt((width-((width/10)*2))-1)+1;
        }

        for (int i =0; i<brojCvetova; i++)
        {
            pozicijaY[i] = rnd.nextInt((height/2)-1)+1;
        }
    }
    //endregion rastrkajCvetove()

    // region onClick
    @Override
    public void onClick(View v) {    }
    //endregion onClick

    //region postaviProbni()
    private void postaviProbni() {
        RelativeLayout.LayoutParams paramsZaPButton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsZaPButton.width=visinaIButtona;
        paramsZaPButton.height=visinaIButtona;
        paramsZaPButton.setMargins(5,5,0,0);
        probni =  new ImageView(this);
        probni = new ImageView(this);
        probni.setLayoutParams(paramsZaPButton);
        probni.setImageResource(R.drawable.cvetmono);
        probni.setScaleType(ImageView.ScaleType.FIT_XY);
        probni.setX(1050);
        probni.setY(380);
        gornjiProstor.addView(probni);
    }
    //endregion postaviProbni

    //region greskaPonovo()
    private void Ponovo(String slika)
    {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);

        RelativeLayout.LayoutParams paramsRefresh = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsRefresh.addRule(RelativeLayout.CENTER_IN_PARENT);

        gornjiProstor.removeAllViews();
        greska = new ImageView(this);
        if (slika == "bravo"){greska.setImageResource(R.drawable.bravo);}
        if (slika == "greska"){greska.setImageResource(R.drawable.greska);}
        greska.setScaleType(ImageView.ScaleType.CENTER);
        greska.setY(5);
        greska.setLayoutParams(params);
        gornjiProstor.addView(greska);

        refresh = new ImageView(this);
        refresh.setImageResource(R.drawable.refresh);
        refresh.setScaleType(ImageView.ScaleType.CENTER);
        refresh.setLayoutParams(paramsRefresh);
        refresh.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Random r = new Random();
                brojCvetova = r.nextInt(9 - 1) + 1;
                for (int a = 0; a < 9; a++)
                {
                    cvetJeUBoji[a] = false;
                }
                for (int a = 0; a<brojCvetova;a++)
                {
                    cvetJeUBoji[a]=true;
                }
                gornjiProstor.removeAllViews();
                rastrkajCvetove();
                iskljuciUkljuciButtone(false);  // iskljuceni brojevi
                staviCvetove();

            }
        });
        gornjiProstor.addView(refresh);
    }
    //endregion


}
